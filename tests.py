from processing import add

def test1():
    return 2 == add(1, 1)

def test2():
    return 0 == add()

def test3():
    return 5 == add(2, 3)

def test4():
    return 3 == add(2, 1)

if __name__ == '__main__':
    with open('result.txt', 'w') as f:
        f.write('tests have started\n')
        if test1():
            f.write('test 1 has passed\n')
        if test2():
            f.write('test 2 has passed\n')
        if test3():
            f.write('test 3 has passed\n')
        if test4():
            f.write('test 4 has passed\n')
        f.write('Success!\n')
    f.close()